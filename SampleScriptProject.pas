{..............................................................................}
{Get path of this script project. Expects the project name in constant
 constScriptProjectName, which must be defined elsewhere. Returns empty string
 if something fails.}
{..............................................................................}
function ScriptProjectPath() : String;
var
  Workspace : IWorkspace;
  Project   : IProject;
  scriptsPath : TDynamicString;
  projectCount : Integer;
  i      : Integer;
  constScriptProjectName : String;
begin
  constScriptProjectName := 'SampleScript';
  { Attempt to get reference to current workspace. }
  Workspace  := GetWorkspace;
  if (Workspace = nil) then begin return:='no workspace'; exit; end;

  { Get a count of the number of currently opened projects.  The script project
   from which this script runs must be one of these. }
  projectCount := Workspace.DM_ProjectCount();

  { Loop over all the open projects.  We're looking for constScriptProjectName
    (of which we are a part).  Once we find this, we want to record the
    path to the script project directory. }
  scriptsPath:='';
  for i:=0 to projectCount-1 do
  begin
    { Get reference to project # i. }
    Project := Workspace.DM_Projects(i);
    { See if we found our script project. }
    if (AnsiPos(constScriptProjectName, Project.DM_ProjectFullPath) > 0) then
    begin
      { Strip off project name to give us just the path. }
      scriptsPath := StringReplace(Project.DM_ProjectFullPath, '\' +
      constScriptProjectName + '.PrjScr','', MkSet(rfReplaceAll,rfIgnoreCase));
    end;
  end;
  result := scriptsPath;
end;

Begin
ScriptProjectPath();

End
