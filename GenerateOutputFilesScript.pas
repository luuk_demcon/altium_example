function ScriptProjectPath() : String;
var
  Workspace : IWorkspace;
  Project   : IProject;
  scriptsPath : TDynamicString;
  projectCount : Integer;
  i      : Integer;
  constScriptProjectName : String;
begin
  constScriptProjectName := 'SampleScript';
  { Attempt to get reference to current workspace. }
  Workspace  := GetWorkspace;
  if (Workspace = nil) then begin return:='no workspace'; exit; end;

  { Get a count of the number of currently opened projects.  The script project
   from which this script runs must be one of these. }
  projectCount := Workspace.DM_ProjectCount();

  { Loop over all the open projects.  We're looking for constScriptProjectName
    (of which we are a part).  Once we find this, we want to record the
    path to the script project directory. }
  scriptsPath:='';
  for i:=0 to projectCount-1 do
  begin
    { Get reference to project # i. }
    Project := Workspace.DM_Projects(i);
    { See if we found our script project. }
    if (AnsiPos(constScriptProjectName, Project.DM_ProjectFullPath) > 0) then
    begin
      { Strip off project name to give us just the path. }
      scriptsPath := StringReplace(Project.DM_ProjectFullPath, '\' +
      constScriptProjectName + '.PrjScr','', MkSet(rfReplaceAll,rfIgnoreCase));
    end;
  end;
  result := scriptsPath;
end;

Procedure GenerateOutputFiles;
Var
    ProjectFilePath   : String;
    WS            : IWorkspace;
    Prj           : IProject;
    Document      : IServerDocument;
Begin
    // Set the Project file path
    ProjectFilePath := ScriptProjectPath() + '\DE118_Timer.PrjPcb';

    // Reset all parameters
    ResetParameters;

    // Open the project
    AddStringParameter('ObjectKind','Project');
    AddStringParameter('FileName', ProjectFilePath);
    RunProcess('WorkspaceManager:OpenObject');
    ResetParameters;

    // Requirement: OutJob file name is Build.OutJob and is exists within the project
    Document := Client.OpenDocument('OUTPUTJOB', ExtractFilePath(ProjectFilePath) + 'Job.OutJob');
    If Document <> Nil Then
    Begin
        WS := GetWorkspace;
        If WS <> Nil Then
        Begin
            Prj := WS.DM_FocusedProject;
            If Prj <> Nil Then
            Begin
                // Compile the project
                Prj.DM_Compile;
                Client.ShowDocument(Document);
                // Set up Output Job Parameters for PDF and generate
                AddStringParameter('ObjectKind', 'OutputBatch');
                AddStringParameter('DisableDialog', 'True');
                AddStringParameter('OutputMedium', 'Documentation');
                AddStringParameter('Action', 'PublishToPDF');
                RunProcess('WorkSpaceManager:Print');
                ResetParameters;
                // Run Output Job
                AddStringParameter('ObjectKind', 'OutputBatch');
                AddStringParameter('Action', 'Run');
                RunProcess('WorkSpaceManager:GenerateReport');


            End;
        End;
    End;

    // Close all objects
    AddStringParameter('ObjectKind','All');
    RunProcess('WorkspaceManager:CloseObject');
    ResetParameters;

    // Close Altium Designer
    TerminateWithExitCode(0);
End;

